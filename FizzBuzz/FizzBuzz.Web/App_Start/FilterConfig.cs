﻿//-----------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web
{
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Filter configuration.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register global filters.
        /// </summary>
        /// <param name="filters">Parameter filter</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}