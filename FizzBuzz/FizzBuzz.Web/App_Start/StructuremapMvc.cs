//-----------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Web.Http;
using System.Web.Mvc;
using FizzBuzz.Web.DependencyResolution;
using StructureMap;

[assembly: WebActivator.PreApplicationStartMethod(typeof(FizzBuzz.Web.App_Start.StructuremapMvc), "Start")]

namespace FizzBuzz.Web.App_Start
{
    /// <summary>
    /// Structure map.
    /// </summary>
    public static class StructuremapMvc
    {
        /// <summary>
        /// Container initialization.
        /// </summary>
        public static void Start()
        {
            IContainer container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}