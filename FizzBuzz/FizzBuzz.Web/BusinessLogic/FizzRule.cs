﻿//-----------------------------------------------------------------------
// <copyright file="FizzRule.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.BusinessLogic
{
    using System;

    /// <summary>
    /// Class which implemented fizz rule.
    /// </summary>
    public class FizzRule : RuleBase
    {
        /// <summary>
        /// Variable initialization for DayOfWeek.
        /// </summary>
        private DayOfWeek dayOfWeek;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzRule"/> class.
        /// </summary>
        /// <param name="nextRule">Parameter to navigate to the next rule in the chain.</param>
        /// <param name="dayOfWeek">parameter to check the day</param>
        public FizzRule(RuleBase nextRule, DayOfWeek dayOfWeek) : base(nextRule)
        {
            this.dayOfWeek = dayOfWeek;
        }

        /// <summary>
        /// checking the fizz rule.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule.</param>
        /// <returns>Returns bool.</returns>
        protected override bool IsApplicable(int number)
        {
            return number % 3 == 0;
        }

        /// <summary>
        /// Get the output if rule is satisfied.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method.</param>
        /// <returns>Returns string</returns>
        protected override string GetOutput(int number)
        {
            if (this.dayOfWeek == DayOfWeek.Wednesday)
            {
                return "wizz";
            }

            return "fizz";
        }
    }
}