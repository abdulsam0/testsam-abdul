﻿//-----------------------------------------------------------------------
// <copyright file="RuleBase.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.BusinessLogic
{
    /// <summary>
    /// Base rule. Which take care of executing the chain of rules.
    /// </summary>
    public abstract class RuleBase
    {
        /// <summary>
        /// Variable initialization for RuleBase.
        /// </summary>
        private RuleBase nextRule;

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleBase"/> class.
        /// </summary>
        /// <param name="nextRule">Parameter to navigate to the next rule in the chain.</param>
        public RuleBase(RuleBase nextRule)
        {
            this.nextRule = nextRule;
        }

        /// <summary>
        /// This method is used to execute the rule.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method.</param>
        /// <returns>Returns String</returns>
        public virtual string Execute(int number)
        {
            if (this.IsApplicable(number))
            {
                return this.GetOutput(number);
            }

            if (this.nextRule != null)
            {
                return this.nextRule.Execute(number);
            }

            return null;
        }

        /// <summary>
        /// Check whether the rule is applicable.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule</param>
        /// <returns>Returns string.</returns>
        protected abstract bool IsApplicable(int number);

        /// <summary>
        /// Get the output if rule is satisfied.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method</param>
        /// <returns>Returns string.</returns>
        protected abstract string GetOutput(int number);
    }
}