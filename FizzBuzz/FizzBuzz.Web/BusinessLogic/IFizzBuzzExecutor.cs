﻿//-----------------------------------------------------------------------
// <copyright file="IFizzBuzzExecutor.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface IFizzBuzzExecutor.
    /// </summary>
    public interface IFizzBuzzExecutor
    {
        /// <summary>
        /// This method is used to loop the chain of rules.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method.</param>
        /// <returns>Returns list</returns>
        IList<string> GetFizzBuzzOutput(int number);
    }
}
