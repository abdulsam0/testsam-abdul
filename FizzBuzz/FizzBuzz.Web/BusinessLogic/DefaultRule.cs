﻿//-----------------------------------------------------------------------
// <copyright file="DefaultRule.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.BusinessLogic
{
    /// <summary>
    /// Class which implemented default rule.
    /// </summary>
    public class DefaultRule : RuleBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultRule"/> class.
        /// </summary>
        public DefaultRule() : base(null)
        {
        }

        /// <summary>
        /// Check the Default rule.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule.</param>
        /// <returns>Returns bool</returns>
        protected override bool IsApplicable(int number)
        {
            return true;
        }

        /// <summary>
        /// Get the output if rule is satisfied.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method.</param>
        /// <returns>Returns string.</returns>
        protected override string GetOutput(int number)
        {
            return number.ToString();
        }
    }
}