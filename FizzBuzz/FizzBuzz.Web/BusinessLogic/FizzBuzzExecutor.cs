﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzExecutor.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// Fizz buzz executor.
    /// </summary>
    public class FizzBuzzExecutor : IFizzBuzzExecutor
    {
        /// <summary>
        /// Variable initialization for abstract class RuleBase.
        /// </summary>
        private RuleBase mainRule;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzExecutor"/> class.
        /// </summary>
        /// <param name="mainRule">Variable used to invoke the instance</param>
        public FizzBuzzExecutor(RuleBase mainRule)
        {
            this.mainRule = mainRule;
        }

        /// <summary>
        /// This method is used to loop the chain of process.
        /// </summary>
        /// <param name="number">Parameter passing to execute the method.</param>
        /// <returns>Returns list</returns>
        public IList<string> GetFizzBuzzOutput(int number)
        {
            var fizzBuzz = new List<string>();

            for (int count = 1; count <= number; count++)
            {
                fizzBuzz.Add(this.mainRule.Execute(count));
            }

            return fizzBuzz;
        }
    }
}