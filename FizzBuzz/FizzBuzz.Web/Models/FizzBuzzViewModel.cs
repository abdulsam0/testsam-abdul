﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzViewModel.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using PagedList;
   
    /// <summary>
    /// Model to view the output
    /// </summary>
    public class FizzBuzzViewModel
    {
        /// <summary>
        /// Gets or sets parameter number.
        /// </summary>
        [Required(ErrorMessage = "Please enter a number")]
        [Display(Name = "Enter Number")]
        [Range(1, 1000, ErrorMessage = "Enter a vaild number. Number should be in the range of 1 to 1000")]

        public int Number { get; set; }

        /// <summary>
        /// Gets or sets list FizzBuzzOutputList
        /// </summary>
        public IPagedList<string> FizzBuzzOutput { get; set; }
    }
}