//-----------------------------------------------------------------------
// <copyright file="Ioc.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.DependencyResolution
{
    using System;
    using BusinessLogic;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// This class is used to implement the method which register all classes in structure map
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// This method is used to implement the concept structure map
        /// </summary>
        /// <returns> ObjectFactory container</returns>
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize((Action<IInitializationExpression>)(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
                x.For<RuleBase>().Add<FizzRule>().Named("FizzRule").Ctor<RuleBase>("nextRule").Named("BuzzRule").Ctor<DayOfWeek>("dayOfWeek").Is(DateTime.Today.DayOfWeek);
                x.For<RuleBase>().Add<BuzzRule>().Named("BuzzRule").Ctor<RuleBase>("nextRule").Named("DefaultRule").Ctor<DayOfWeek>("dayOfWeek").Is(DateTime.Today.DayOfWeek);
                x.For<RuleBase>().Add<DefaultRule>().Named("DefaultRule");
                x.For<RuleBase>().Use<FizzBuzzRule>().Ctor<RuleBase>("nextRule").Named("FizzRule").Ctor<DayOfWeek>("dayOfWeek").Is(DateTime.Today.DayOfWeek);
                x.For<IFizzBuzzExecutor>().Use<FizzBuzzExecutor>();
            }));
            return ObjectFactory.Container;
        }
    }
}