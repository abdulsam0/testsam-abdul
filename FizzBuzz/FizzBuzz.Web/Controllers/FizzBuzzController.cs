﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Controllers
{
    using System.Web.Mvc;
    using BusinessLogic;
    using Models;
    using PagedList;
    
    /// <summary>
    /// Fizz buzz controller.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Object creation
        /// </summary>
        private readonly IFizzBuzzExecutor fizzBuzz;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class.
        /// </summary>
        /// <param name="fizzBuzz">Instance created to invoke the method</param>
        public FizzBuzzController(IFizzBuzzExecutor fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        /// <summary>
        /// Action result Index
        /// </summary>
        /// <returns>Return FizzBuzzViewModel</returns>
        [HttpGet]
        public ActionResult Index()
        {
            var model = new FizzBuzzViewModel();
            return this.View(model);
        }

        /// <summary>
        /// Action result FizzBuzzOutput.
        /// </summary>
        /// <param name="model"> Instance to get the properties</param>
        /// <param name="page"> variable to get the pages</param>
        /// <returns>Returns output of fizz buzz list</returns>
        public ActionResult FizzBuzzOutput(FizzBuzzViewModel model, int page = 1)
        {
            if (this.ModelState.IsValid)
            {
                int pageSize = 20;
                var fizzBuzzOutput = this.fizzBuzz.GetFizzBuzzOutput(model.Number);
                model.FizzBuzzOutput = fizzBuzzOutput.ToPagedList(page, pageSize);
                return this.View("Index", model);
            }

            return this.View("Index", model);
        }
    }
}