﻿//-----------------------------------------------------------------------
// <copyright file="BuzzRuleTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using System;
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Buzz rule tests.
    /// </summary>
    [TestFixture]
    public class BuzzRuleTests
    {
        /// <summary>
        /// Buzz rule output test
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule.</param>
        /// <param name="dayOfWeek">parameter passing to check the day.</param>
        /// <param name="expectedResult">Expected result</param>
        [TestCase(5, DayOfWeek.Monday, "buzz")]
        [TestCase(10, DayOfWeek.Monday, "buzz")]
        [TestCase(3, DayOfWeek.Monday, null)]
        [TestCase(5, DayOfWeek.Wednesday, "wuzz")]
        [TestCase(10, DayOfWeek.Wednesday, "wuzz")]
        [TestCase(2, DayOfWeek.Wednesday, null)]
        public void BuzzOutputTest(int number, DayOfWeek dayOfWeek, string expectedResult)
        {
            var rule = new Mock<RuleBase>(null);

            var buzzRule = new BuzzRule(rule.Object, dayOfWeek);
            var result = buzzRule.Execute(number);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
