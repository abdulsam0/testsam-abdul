﻿//-----------------------------------------------------------------------
// <copyright file="FizzRuleTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using System;
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Fizz rule test. 
    /// </summary>
    [TestFixture]
    public class FizzRuleTests
    {
        /// <summary>
        /// Fizz rule test.
        /// </summary>
        /// <param name="number">parameter passing to execute the rule</param>
        /// <param name="dayOfWeek">parameter passing to check the day</param>
        /// <param name="expectedResult">Expected result</param>
        [TestCase(3, DayOfWeek.Monday, "fizz")]
        [TestCase(6, DayOfWeek.Monday, "fizz")]
        [TestCase(2, DayOfWeek.Monday, null)]
        [TestCase(3, DayOfWeek.Wednesday, "wizz")]
        [TestCase(6, DayOfWeek.Wednesday, "wizz")]
        [TestCase(4, DayOfWeek.Wednesday, null)]
        public void FizzOutputTest(int number, DayOfWeek dayOfWeek, string expectedResult)
        {
            var rule = new Mock<RuleBase>(null);

            var fizzRule = new FizzRule(rule.Object, dayOfWeek);
            var result = fizzRule.Execute(number);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
