﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzRuleTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using System;
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Fizz Buzz rule test.
    /// </summary>
    [TestFixture]
    public class FizzBuzzRuleTests
    {
        /// <summary>
        /// Fizz buzz rule test.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule.</param>
        /// <param name="dayOfWeek">parameter passing to check the day</param>
        /// <param name="expectedResult">Expected result</param>
        [TestCase(15, DayOfWeek.Monday, "fizz buzz")]
        [TestCase(30, DayOfWeek.Monday, "fizz buzz")]
        [TestCase(10, DayOfWeek.Monday, null)]
        [TestCase(15, DayOfWeek.Wednesday, "wizz wuzz")]
        [TestCase(30, DayOfWeek.Wednesday, "wizz wuzz")]
        [TestCase(20, DayOfWeek.Wednesday, null)]
        public void FizzBuzzOutputTest(int number, DayOfWeek dayOfWeek, string expectedResult)
        {
            var rule = new Mock<RuleBase>(null);

            var fizzBuzzRule = new FizzBuzzRule(rule.Object, dayOfWeek);
            var result = fizzBuzzRule.Execute(number);
            Assert.AreEqual(expectedResult, result);
        }        
    }
}
