﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzExecutorTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using System.Collections.Generic;
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Fizz buzz executor test.
    /// </summary>
    [TestFixture]
    public class FizzBuzzExecutorTests
    {
        /// <summary>
        /// Test for output if the number is not divisible by both three and five.
        /// </summary>
        [Test]
        public void GetOutputNotDivisibleByThreeAndFiveTest()
        {
            var rule = new Mock<RuleBase>(null);

            rule.Setup(x => x.Execute(1)).Returns("1");
            var fizzBuzzExecutor = new FizzBuzzExecutor(rule.Object);
            var list = fizzBuzzExecutor.GetFizzBuzzOutput(1);
            Assert.AreEqual("1", list[0]);
        }

        /// <summary>
        /// Test for output if the number is  divisible by both three and five.
        /// </summary>
        [Test]
        public void GetOutputDivisibleByThreeAndFiveTest()
        {
            var rule = new Mock<RuleBase>(null);

            rule.Setup(x => x.Execute(15)).Returns("fizz buzz");
            var fizzBuzzExecutor = new FizzBuzzExecutor(rule.Object);
            var list = fizzBuzzExecutor.GetFizzBuzzOutput(15);
            Assert.AreEqual("fizz buzz", list[14]);
        }

        /// <summary>
        /// Test for output if the number is divisible by three.
        /// </summary>
        [Test]
        public void GetOutputDivisibleByThreeTest()
        {           
            var rule = new Mock<RuleBase>(null);

            rule.Setup(x => x.Execute(3)).Returns("fizz");
            var fizzBuzzExecutor = new FizzBuzzExecutor(rule.Object);
            var list = fizzBuzzExecutor.GetFizzBuzzOutput(3);
            Assert.AreEqual("fizz", list[2]);
        }

        /// <summary>
        /// Test for output if the number is  divisible by five.
        /// </summary>
        [Test]
        public void GetOutputDivisibleByFiveTest()
        {
            var rule = new Mock<RuleBase>(null);

            rule.Setup(x => x.Execute(5)).Returns("buzz");
            var fizzBuzzExecutor = new FizzBuzzExecutor(rule.Object);
            var list = fizzBuzzExecutor.GetFizzBuzzOutput(5);
            Assert.AreEqual("buzz", list[4]);
        }
    }
}
