﻿//-----------------------------------------------------------------------
// <copyright file="TestRule.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using BusinessLogic;
    using FizzBuzz.Web.BusinessLogic;

    /// <summary>
    /// Test Rule inherited from BaseRule
    /// </summary>
    public class TestRule : RuleBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestRule"/> class.
        /// </summary>
        /// <param name="nextRule">Parameter to navigate to the next rule in the chain.</param>
        public TestRule(RuleBase nextRule) : base(nextRule)
        {
        }

        /// <summary>
        /// Check the rule is applicable.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule</param>
        /// <returns>Returns bool</returns>
        protected override bool IsApplicable(int number)
        {
            return number % 2 == 0;
        }

        /// <summary>
        /// Get the output if rule is satisfied.
        /// </summary>
        /// <param name="number">Parameter passing execute the method.</param>
        /// <returns>Returns string.</returns>
        protected override string GetOutput(int number)
        {
            return "Test";
        }
    }
}
