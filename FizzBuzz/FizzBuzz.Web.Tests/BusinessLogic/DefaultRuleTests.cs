﻿//-----------------------------------------------------------------------
// <copyright file="DefaultRuleTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Default rule.
    /// </summary>
    [TestFixture]
    public class DefaultRuleTests
    {
        /// <summary>
        /// Default rule test.
        /// </summary>
        /// <param name="number">Parameter passing to execute the rule.</param>
        /// <param name="expectedResult">Expected result.</param>
        [TestCase(8, "8")]
        [TestCase(11, "11")]
        public void DefaultRuleOutputTest(int number, string expectedResult)
        {
            var defaultRule = new DefaultRule();
            var result = defaultRule.Execute(number);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
