﻿//-----------------------------------------------------------------------
// <copyright file="RuleBaseTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.BusinessLogic
{
    using FizzBuzz.Web.BusinessLogic;
    using Moq;
    using NUnit.Framework;
    
    /// <summary>
    /// Rule base test.
    /// </summary>
    [TestFixture]
    public class RuleBaseTests
    {
        /// <summary>
        /// Return result if rule is satisfied.
        /// </summary>
        [Test]
        public void ShouldReturnResultIfRuleSatisifies()
        {
            var result = new TestRule(null).Execute(2);
            Assert.AreEqual(result, "Test");
        }

        /// <summary>
        /// Execute next rule if rule is not satisfied.
        /// </summary>
        [Test]
        public void ShouldExecuteNextRuleWhenCurrentRuleNotApplicable()
        {
            var nextRule = new Mock<RuleBase>(null);
            nextRule.Setup(p => p.Execute(It.IsAny<int>())).Returns("NextResult");
            var result = new TestRule(nextRule.Object).Execute(3);
            Assert.AreEqual(result, "NextResult");
        }

        /// <summary>
        /// Return null if next rule is null.
        /// </summary>
        [Test]
        public void ShouldReturnNullWhenNextRuleIsNull()
        {
            var result = new TestRule(null).Execute(3);
            Assert.AreEqual(result, null);
        }
    }
}
