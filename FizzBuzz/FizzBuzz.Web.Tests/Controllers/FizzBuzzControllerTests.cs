﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTests.cs" company="Tata Consultancy Services">
//     Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FizzBuzz.Web.Tests.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Web.BusinessLogic;
    using FizzBuzz.Web.Controllers;
    using FizzBuzz.Web.Models;
    using Moq;
    using NUnit.Framework;
    using PagedList;
    
    /// <summary>
    /// Fizz buzz controller tests.
    /// </summary>
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// This method will test the view of action result Index.
        /// </summary>
        [Test]
        public void IndexTest()
        {
            var rule = new Mock<RuleBase>(null);
            rule.Setup(x => x.Execute(2)).Returns("2");

            var controller = new FizzBuzzController(new FizzBuzzExecutor(rule.Object));
            var result = controller.Index() as ViewResult;
            Assert.IsInstanceOf(typeof(FizzBuzzViewModel), result.Model);
        }

        /// <summary>
        /// This method will test the view of action result FizzBuzzOutput.
        /// </summary>
        [Test]
        public void FizzBuzzOutputTest()
        {
            var mockExecutor = new Mock<IFizzBuzzExecutor>();
            mockExecutor.Setup(x => x.GetFizzBuzzOutput(5)).Returns(new List<string> { "1", "2", "Fizz", "4", "Buzz" });
            var viewModel = new FizzBuzzViewModel
            {
                Number = 5
            };

            var controller = new FizzBuzzController(mockExecutor.Object);
            var result = controller.FizzBuzzOutput(viewModel) as ViewResult;
            Assert.AreEqual(result.ViewName, "Index");
            Assert.IsInstanceOf(typeof(FizzBuzzViewModel), result.Model);

            var fizzBuzzViewModel = result.Model as FizzBuzzViewModel;
            Assert.IsInstanceOf(typeof(IPagedList<string>), fizzBuzzViewModel.FizzBuzzOutput);
            Assert.AreEqual(5, fizzBuzzViewModel.FizzBuzzOutput.TotalItemCount);
        }
    }
}
